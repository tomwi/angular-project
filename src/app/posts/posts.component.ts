import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {



posts;
isloadnig:Boolean = true;

  constructor(private _postsService:PostsService) { } 
// decalring an object postservice from type postservice.
// to allow us to make changes in "ngOnInit". 

  deletePost(post){  
  this._postsService.deletePost(post);
    
  }
  editPost(post){
    this._postsService.updatePost(post)
  }
  showPost(post){
    this.posts
  }
addPost(post){ // // sends the post to add post method in the postservice 
  this._postsService.addPost(post)

}

  ngOnInit() {
      

     // left usersData its the input of the function.
      this._postsService.getPosts().subscribe(postsData => {this.posts = postsData ; this.isloadnig = false ;   console.log(this.posts)})
        // end of spinner  
      // subscribe: to pull the data from observable, inside there is arrow notation.
  }                                                                                 //  function(postdata) {
                                                                                    //  this.posts = postdata }
}
