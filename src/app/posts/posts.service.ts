import { Injectable } from '@angular/core';
import {Http} from '@angular/http'; // import http for pulling data from sever.
import 'rxjs/add/operator/map'; // map function allows us to change the format of the file from string to json. //inside we will use arrow notation.
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()

export class PostsService {


postsObservable;

// private _url = 'http://jsonplaceholder.typicode.com/posts' ; //

constructor(private af: AngularFire) { } // declaring on a private attribute from type Http thst was imported. 


  // getPosts(){

  //  return this._http.get(this._url).map( res => res.json()).delay(2000)    //res is just a name 
  //  //  import שהצהרנו עלייה בתוך הבנאי ועשינו   Http  בשורה למעלה השתמשנו בתכונה  
  // //  ,  URL כדי לשלוף את המידע מתוך ה observable שמחזירה get לאחר מכן השתמשנו ב 
  //  // json כדי להפוך מ סטרינג ל  map נשתמש ב   observable לאחר מכן כדי לשנות את סוג הפורמט מה 
  // }

//--------------------------------------------------------------------------
//(למה אנחנו שולחים לכאן את המטודה ורק מפה לשרת ואיך זה מתבטא)
//--------------------------------------------------------------------------


addPost(post){ // gets post from add post in posts.component and pushes it 
 this.postsObservable.push(post); 
}
deletePost(post){
  this.af.database.object('/posts/' + post.$key).remove();
 console.log(post); // this. refers to property in the component. 
}

  getPosts(){
//reading data from firebase //// to read spacific data  from db we can refer it to /users/1  // same as sql query
this.postsObservable = this.af.database.list('/posts').map(
  posts => {
    posts.map(
        post =>{
            post.name = [];
            post.email = [];
            for(var p in post.users){
              post.name.push(
                this.af.database.object('/users/' + p)
              )
                post.email.push(
                this.af.database.object('/users/' + p)
              )
            }
        }
    );
  return posts;
}

)

return this.postsObservable;
// return this._http.get(this._url).map( res => res.json()).delay(2000)
    }

    // the mtehod takes the new values and pushes it to firebase with the uniqe key
      updatePost(post){
    let post1 = {title:post.title}
    console.log(post1);               //key of the new post   
    this.af.database.object('/posts/' + post.$key).update(post1)
  }
}
