import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {DataTableModule} from "angular2-datatable";
import { RouterModule, Routes } from '@angular/router'; // for routes
import{AngularFireModule} from 'angularfire2';



import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import {UsersService} from './users/users.service';
import { PostsComponent } from './posts/posts.component';
import { PostComponent } from './post/post.component';
import { PostsService } from './posts/posts.service';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


 var config = {
    apiKey: "AIzaSyDcklXpHku73Amn3XckPK4AXGkeLMY9ku0",
    authDomain: "angular-home-5c92e.firebaseapp.com",
    databaseURL: "https://angular-home-5c92e.firebaseio.com",
    storageBucket: "angular-home-5c92e.appspot.com",
    messagingSenderId: "423625281122"
 };


const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: '', component: PostsComponent },
  { path: '**', component: PageNotFoundComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    UserFormComponent,
    PageNotFoundComponent,
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DataTableModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(config)

  ],
  providers: [UsersService, PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
 

