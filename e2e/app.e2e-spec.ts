import { AlgularprojectPage } from './app.po';

describe('algularproject App', function() {
  let page: AlgularprojectPage;

  beforeEach(() => {
    page = new AlgularprojectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
